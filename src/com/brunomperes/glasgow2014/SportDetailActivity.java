package com.brunomperes.glasgow2014;

import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;

/**
 * An activity representing a single Sport detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link SportListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link SportDetailFragment}.
 */
public class SportDetailActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sport_detail);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(SportDetailFragment.ARG_ITEM_ID, getIntent()
					.getStringExtra(SportDetailFragment.ARG_ITEM_ID));
			SportDetailFragment fragment = new SportDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
			.add(R.id.sport_detail_container, fragment).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpTo(this, new Intent(this,
					SportListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void watchOutForButton(View v){
		new AlertDialog.Builder(this).setTitle("Watch out for").setMessage(Data.sports.get(SportDetailFragment.idSelected).watchOutFor).setNeutralButton("Close", null).show();
	}

	public void pastWinnersButton(View v){
		Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(Data.sports.get(SportDetailFragment.idSelected).pastWinnersLink));
		startActivity(browserIntent);
	}

	public void venuesButton(View v){
		DialogInterface.OnClickListener mapHandler = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				String uri = String.format(Locale.ENGLISH, "geo:" + Data.sports.get(SportDetailFragment.idSelected).venueMap);
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				startActivity(intent);
				
			}
		};
		new AlertDialog.Builder(this)
				.setTitle("Venue info")
				.setMessage(
						Data.sports.get(SportDetailFragment.idSelected).venueInfo)
				.setNeutralButton("Close", null)
				.setPositiveButton("View Map", mapHandler).show();
	}
}
