package com.brunomperes.glasgow2014;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A fragment representing a single Sport detail screen. This fragment is either
 * contained in a {@link SportListActivity} in two-pane mode (on tablets) or a
 * {@link SportDetailActivity} on handsets.
 */
public class SportDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private Sport mItem;
	public static int idSelected;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public SportDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.

			idSelected = Integer.parseInt(getArguments().getString(
					ARG_ITEM_ID));
			mItem = Data.sports.get(idSelected);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_sport_tab_dummy,
				container, false);

		// Select the view elements to replace for the current sport
		TextView sportName = (TextView) rootView.findViewById(R.id.sportName);
		ImageView sportImage = (ImageView) rootView
				.findViewById(R.id.sportImage);
		TextView sportRules = (TextView) rootView.findViewById(R.id.sportRules);

		// Sets the content for the current sport
		Sport currentSport = mItem;
		sportImage.setImageResource(currentSport.imageId);
		sportName.setText(currentSport.name);
		sportRules.setText(currentSport.info);

		return rootView;
	}
}
