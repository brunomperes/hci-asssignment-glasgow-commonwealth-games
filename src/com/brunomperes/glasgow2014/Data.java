package com.brunomperes.glasgow2014;

import java.util.ArrayList;

public class Data {

	public static ArrayList<Sport> sports = null;

	public static ArrayList<Sport> generateSports() {

		sports = new ArrayList<Sport>();

		sports.add(new Sport(
				"0",
				"Athletics",
				"Athletics is amongst the most ancient of human contests.\n\nThe world-class athletes in this competition are determined to run faster, jump higher and throw further than anyone else.\n\nAt Glasgow 2014, the Athletics disciplines of Track, Field and Marathon will feature across seven days of competition.\n\nWorld and Olympic champions will be among those competing across sprints, middle-distance, long-distance and field events, including events for Para-Sport athletes.\n\nHampden Park will be transformed from football stadium to international-standard Track and Field facility, where all eyes will focus on the athletes' blistering pace, agility and strength. The 100 Metre finals will have some of the fastest men and women on the planet racing for Gold.\n\nEndurance will be tested in the Marathon, which will take in the sights of Glasgow as part of a 42.195km city course.\n\nAnything can happen at the Athletics – underdogs and champions line up side-by-side, each determined to win. A fraction of a second or a fraction of a centimetre can turn an unknown athlete into a star.",
				"http://en.wikipedia.org/wiki/Athletics_at_the_Commonwealth_Games",
				"The strength of athletes from across the Commonwealth will be on display. Whether through the sprint dominance of the Jamaicans, the brilliant distance endurance of the African nations or the usual Track and Field medal hauls for England, Australia, Canada and New Zealand, the week-long display of prowess at the Glasgow 2014 Athletics will bring forth a host of new stars.",
				"Hampden Park Stadium (Track & Field) [South Side]",
				"55.825606,-4.251893", R.drawable.athletics));
		sports.add(new Sport(
				"1",
				"Badminton",
				"Badminton players are the gymnasts of racquet sports. Watch as they lunge, stretch and leap, sending the shuttle flying across the court.\nWidely regarded as the fastest racquet sport in the world, with shuttles travelling at more than 300km/h, Badminton will feature on all 11 days of the Games programme.\n\nSix Gold medal events will take in Men’s and Women’s Singles and Doubles, Mixed Doubles and a combined Mixed Team event.\n\nSingles and Doubles competitions will consist of a straight knockout draw, while the Mixed Team match will be contested over a range of preliminary rounds before a Finals knockout format reveals the eventual winners.\n\nCompetition will take centre stage across six purpose-built courts at the Emirates Arena.",
				"http://en.wikipedia.org/wiki/Badminton_at_the_Commonwealth_Games",
				"Malaysia and England traditionally dominate this sport, which has tens of millions of followers and players around the world. You will also see rising stars from Scotland, India and Singapore who will challenge for the medals in Glasgow.",
				"Emirates Arena [East End]", "55.847265,-4.207037",
				R.drawable.badminton));
		sports.add(new Sport(
				"2",
				"Boxing",
				"Float like a butterfly and sting like a bee. That’s what the boxers will have to do to win Gold at Glasgow 2014.\n\nInside the ring the boxers need strength, skill and speed to outwit their opponent and you can bet the crowd will be roaring themselves hoarse, urging their favourite to land the knockout punch.\n\nGlasgow 2014 is the first time that women’s Boxing will be included on the sport programme at a Commonwealth Games. Female athletes will compete in the Flyweight, Lightweight and Middleweight categories. Medals will be fought for over four rounds of two minutes.\n\nMale athletes in ten weight categories will fight for a medal over three rounds in a “knockout” bout format, across eight days of fierce competition.",
				"http://en.wikipedia.org/wiki/Boxing_at_the_Commonwealth_Games",
				"Historically, England and Canada have been dominant in this sport, with Scotland also winning its fair share of medals. Look out for rising stars from the African nations challenging for Glasgow 2014 Gold in what is set to be a real knockout.",
				"Scottish Exhibition and Conference Centre (SECC) Precint with Finals in the SSE Hydro",
				"55.86173,-4.287685", R.drawable.boxing));
		sports.add(new Sport(
				"3",
				"Hockey",
				"Hockey is a physical game packed with fast, intense action. There’s no time for hanging around – from the very first moment the players are going for goals.\n\n10 nations will be participating in each of the Men’s and Women’s Hockey competitions at Glasgow 2014. In the preliminary rounds, teams will be split into two pools of five. The top two teams in each pool will progress to play off for the medals.\n\nThe Hockey matches will take place at the new Glasgow National Hockey Centre, which boasts two international-standard water-based pitches.",
				"http://en.wikipedia.org/wiki/Hockey_at_the_Commonwealth_Games",
				"Australia has been the dominant force in Commonwealth Games Men’s Hockey, securing every title since its introduction in 1998. Challenges in both competitions from other medal-winning nations including Pakistan, England, India, New Zealand and Malaysia. Australian women have also dominated the Gold medal spot at previous Commonwealth Games. However, they still find themselves below New Zealand and England in the current World Rankings.",
				"Glasgow National Hockey Centre [East End]",
				"55.846362,-4.236616", R.drawable.hockey));
		sports.add(new Sport(
				"4",
				"Judo",
				"Judo, a relatively modern martial art, has grown in popularity throughout the Commonwealth. It is a highly physical sport with a strong ethical code and guiding philosophy.\n\nIn a display of physical strength and tactical positioning, the sport pits two athletes against each other in a contest to control and dictate the movements of the opponent. The aim is to score points by throwing them to the ground or immobilising them with armlocks, holds or strangles.\n\nBoth male and female athletes – dressed in the traditional white and blue Judogi (uniforms) – will compete in 14 defined weight categories in Glasgow, vying for the 14 Gold medals on offer.",
				"http://en.wikipedia.org/wiki/Judo_at_the_Commonwealth_Games",
				"Scotland always puts forward a strong team at the Commonwealth Games and along with England, Wales, Canada and Australia, should lead the charge for medals as the sport returns to the Games programme for the first time since Manchester 2002.",
				"SECC Precinct [West End]", "55.86173,-4.287685",
				R.drawable.judo));
		sports.add(new Sport(
				"5",
				"Lawn Bowls",
				"The setting may be tranquil, but don’t be fooled – this is Lawn Bowls and the stakes are high.\n\nBowlers mix precision with boldness and they play with a focus that soon rubs off on the spectators.\n\nLawn Bowls is strongly represented throughout the Commonwealth, and Glasgow 2014 will feature a number of current World Champions. This sport also offers the most diverse group of competing athletes, from all age groups and backgrounds.\n\nAthletes will compete for eight Gold medals across the men’s and women’s singles, pairs, triples and fours, beginning with a round robin format before knockout finals determine the medal winners.\n\nIntegrated into the lawn bowls programme will be two Para-Sport events for bowlers who are visually impaired or have a physical disability.\n\nSet in one of Glasgow’s most famous parks, the Lawn Bowls competition will take place at the picturesque Kelvingrove Lawn Bowls Centre, adjacent to the renowned Kelvingrove Art Gallery and Museum.",
				"http://en.wikipedia.org/wiki/Lawn_bowls_at_the_Commonwealth_Games",
				"Lawn Bowls has perhaps one of the most evenly spread medal tallies of the Games, with nations including Australia, England, New Zealand, Scotland, South Africa, Malaysia and Wales sharing the spoils. With a raft of young athletes joining the sport throughout the Commonwealth, there may be some surprising results come 2014.",
				"Kelvingrove Lawn Bowls Centre [West End]",
				"55.867895,-4.287979", R.drawable.lawnbowls));
		sports.add(new Sport(
				"6",
				"Netball",
				"Netball lives up to its reputation as a fast and exciting display of athleticism, skill and tactics as the game shifts quickly from defence to attack.\n\nThis is a truly Commonwealth sport where World Championship titles have been in the keeping of Commonwealth nations since their inception.\n\nCompetition will take place at the SECC Precinct where a single Gold medal will be up for grabs amongst the 12 competing nations.",
				"http://en.wikipedia.org/wiki/Netball_at_the_Commonwealth_Games",
				"Australia and New Zealand have battled it out for all four Commonwealth Gold medals up until 2010. But watch out for the challenges to come from England, Jamaica, South Africa and fast-developing Malawi at Glasgow 2014. ",
				"SECC Precinct [West End]", "55.86173,-4.287685",
				R.drawable.netball));
		sports.add(new Sport(
				"7",
				"Rugby Sevens",
				"Rugby Sevens is a fast and furious contest with high point-scoring, hard hits and heroic sprints. It takes two teams of seven men and gives them just 14 minutes to win the game. It’s all about teamwork, skills and stamina and with just seven players per team, everyone’s got a job to do.\n\nThe majority of top Rugby Sevens nations in the world will be expected at Glasgow 2014. With Rugby Sevens set to make its Olympic Games debut in Rio de Janeiro in 2016, the Glasgow 2014 Commonwealth Games will take on a new focus as the best teams in the Commonwealth look to get an early foothold in the sport for the next decade of competition.\n\nA total of 45 games will be contested across two days of fierce competition at Ibrox Stadium, as 16 teams strive in front of packed audiences for that single Gold.",
				"http://en.wikipedia.org/wiki/Rugby_sevens_at_the_Commonwealth_Games",
				"New Zealand has dominated Commonwealth Games Rugby Sevens, claiming all four Gold medals to date. Other competing nations including England, Australia and South Africa are determined to challenge their crown.",
				"Ibrox Stadium [South Side]", "55.853854,-4.308965",
				R.drawable.rugby));
		sports.add(new Sport(
				"8",
				"Squash",
				"Squash is a highly energetic racquet sport that requires outstanding levels of fitness, combined with a deft touch for precise ball placement within the enclosed field of play.\n\nIn this traditionally strong Commonwealth sport, athletes will contest individual Men’s and Women’s Singles events and pair up across Men’s, Women’s and Mixed Doubles competitions, where the width of the court is increased to allow for four players.\n\nCompetition will take place at Scotstoun Sports Campus, where new permanent courts will be utilised in preliminary rounds.\n\nMedal matches will take place under the lens of a purpose-built glass-walled show court, providing spectators with viewing angles from all four sides.\n\nExpect pace, excitement and a masterful display of skills and stamina.",
				"http://en.wikipedia.org/wiki/Squash_at_the_Commonwealth_Games",
				"Since the introduction of Squash to the Games in 1998, Australia and England have fought for the majority of medals, but watch out for talented athletes from Malaysia, New Zealand, India and Pakistan.",
				"Scotstoun Sports Campus [West End]", "55.882725,-4.34236",
				R.drawable.squash));
		sports.add(new Sport(
				"9",
				"Table Tennis",
				"Table Tennis is a spectacle of accuracy and speed, with split-second athlete reactions required to keep the ball in play.\n\nCompetition will take place at the Scotstoun Sports Campus, where up to 10 competition tables, including two main show courts, will see athletes contest singles and doubles events as well as dedicated team events in the quest for Glasgow 2014 Gold.\n\nSince making its debut at the Manchester 2002 Commonwealth Games, Table Tennis remains a strong sport amongst Commonwealth nations, and the spin, speed and skill of the game are sure to be displayed across 10 days of competition in Glasgow.",
				"http://en.wikipedia.org/wiki/Table_tennis_at_the_Commonwealth_Games",
				"Historically, Singapore has been way ahead in this sport, while India, Australia, and England have been consistent performers. Look out for the men's team from Scotland who competed at the Delhi 2010 Commonwealth Games.",
				"Scotstoun Sports Campus [West End]", "55.882725,-4.34236",
				R.drawable.tabletennis));
		sports.add(new Sport(
				"10",
				"Triathlon",
				"Combining three unique and challenging disciplines – swimming, cycling and running – Triathlon demonstrates the overall fitness, skill and endurance of some of the world’s most talented athletes.\n\nAthletes start with a 1,500-metre open-water swim before switching to a 40-kilometre road cycle, finishing with a challenging 10-kilometre run. Depending on an athlete’s individual strength, strategy plays a major role in trying to stay ahead of the pack.\n\nGlasgow 2014 sees the inclusion of a Mixed Relay event for the very first time at the Commonwealth Games. This innovative format consists of two men and two women. Each athlete completes a ‘super-sprint’ triathlon of swimming, biking and running, before tagging off to their next team mate.\n\nTriathlon action will take place in and around the Strathclyde Country Park on the outskirts of Glasgow. It is expected that this energetic and exciting sport will attract large audiences to support the male and female athletes as they strive for Gold medal success.",
				"http://en.wikipedia.org/wiki/Triathlon_at_the_Commonwealth_Games",
				"Australia, New Zealand and Canada have so far toughed it out for Commonwealth Games Triathlon medals and they will most likely be the nations to beat in 2014.",
				"Strathclyde Country Park [Satellite]", "55.792789,-4.01973",
				R.drawable.triathlon));
		sports.add(new Sport(
				"11",
				"Weightlifting",
				"Weightlifting requires gravity-defying strength, as male and female athletes heft huge weights into the air with every lift.\n\nIt is a competition of physical and mental toughness, where athletes compete according to 15 pre-determined body-weight categories and must record the biggest lift in their division to claim the Gold medal.\n\nThe Games will also include a Powerlifting competition for Para-Sport athletes, where weights will be bench-pressed into the air from a stationary position, with individual bodyweight and technique playing a major role in determining the overall medallists.\n\nSpectators will experience the intensity and drama of this competition at the SECC Precinct.",
				"http://en.wikipedia.org/wiki/Weightlifting_at_the_Commonwealth_Games",
				"Nigeria pulled the biggest haul at Delhi 2010, and nations including Australia, England, Canada, India and Wales are usually among the medal winners. Watch out for big improvements from young up-and-coming lifters from nations within the Oceania, Asian and African regions.",
				"SECC Precinct [West End]", "55.86173,-4.287685",
				R.drawable.weightlifting));
		sports.add(new Sport(
				"12",
				"Wrestling",
				"The sport of Wrestling involves a classic contest between two athletes seeking ascendancy through a range of grapples, holds, throws and pins.\n\nA sport with an ancient tradition, Wrestling is a competition of strength, suppleness and raw power. Athletes need excellent focus to outwit their opponent.\n\nGlasgow 2014 Commonwealth Games Wrestling will take place at the SECC Precinct in the Freestyle discipline for both men and women.",
				"http://en.wikipedia.org/wiki/Wrestling_at_the_Commonwealth_Games",
				"Canada has been the dominant Commonwealth nation in Games Wrestling, while India has proven its credentials in recent Games competitions. With a raft of medals for the taking, athletes from around the globe will be looking to claim their own piece of Games history.",
				"SECC Precinct [West End]", "55.86173,-4.287685",
				R.drawable.wrestling));

		return sports;

	}

	/**
	 * Returns a string array with the name of all sports initialized
	 * 
	 * @return
	 */
	public static String[] getSportsNames() {

		if (sports == null) {
			generateSports();
		}

		int countSports = sports.size();
		String[] names = new String[countSports];

		for (int i = 0; i < countSports; i++) {
			names[i] = sports.get(i).name;
		}

		return names;
	}

}
