package com.brunomperes.glasgow2014;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SportTabActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sport_tab);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sport_tab, menu);
		return true;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			Fragment fragment = new DummySectionFragment();
			Bundle args = new Bundle();
			args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return Data.sports.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Sport s = Data.sports.get(position);
			if (s != null) {
				return s.name;
			}
			return "";
		}
	}
	
	public void watchOutForButton(View v){
		new AlertDialog.Builder(this).setTitle("Watch out for").setMessage(Data.sports.get(SportDetailFragment.idSelected).watchOutFor).setNeutralButton("Close", null).show();
	}

	public void pastWinnersButton(View v){
		Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse(Data.sports.get(SportDetailFragment.idSelected).pastWinnersLink));
		startActivity(browserIntent);
	}

	public void venuesButton(View v){
		DialogInterface.OnClickListener mapHandler = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				String uri = String.format(Locale.ENGLISH, "geo:" + Data.sports.get(SportDetailFragment.idSelected).venueMap);
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
				startActivity(intent);
				
			}
		};
		new AlertDialog.Builder(this)
				.setTitle("Venue info")
				.setMessage(
						Data.sports.get(SportDetailFragment.idSelected).venueInfo)
				.setNeutralButton("Close", null)
				.setPositiveButton("View Map", mapHandler).show();
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			// Gets the number of the section being shown
			int sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);

			View rootView = inflater.inflate(R.layout.fragment_sport_tab_dummy,
					container, false);

			// Select the view elements to replace for the current sport
			TextView sportName = (TextView) rootView
					.findViewById(R.id.sportName);
			ImageView sportImage = (ImageView) rootView
					.findViewById(R.id.sportImage);
			TextView sportRules = (TextView) rootView
					.findViewById(R.id.sportRules);

			// Sets the content for the current sport
			Sport currentSport = Data.sports.get(sectionNumber);
			sportImage.setImageResource(currentSport.imageId);
			sportName.setText(currentSport.name);
			sportRules.setText(currentSport.info);

			return rootView;
		}
	}

}
