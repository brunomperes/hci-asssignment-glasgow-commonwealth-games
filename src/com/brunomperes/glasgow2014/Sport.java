package com.brunomperes.glasgow2014;

public class Sport {

	public String id;
	public String name;
	public String info;
	public String pastWinnersLink;
	public String watchOutFor;
	public String venueInfo;
	public String venueMap;
	public int imageId;

	/**
	 * 
	 * @param id
	 * @param name
	 * @param info
	 * @param pastWinnersLink
	 * @param watchOutFor
	 * @param venueInfo
	 * @param venueMap
	 * @param imageId
	 */
	public Sport(String id, String name, String info, String pastWinnersLink,
			String watchOutFor, String venueInfo, String venueMap, int imageId) {
		super();
		this.id = id;
		this.name = name;
		this.info = info;
		this.pastWinnersLink = pastWinnersLink;
		this.watchOutFor = watchOutFor;
		this.venueInfo = venueInfo;
		this.venueMap = venueMap;
		this.imageId = imageId;
	}
	
	public String toString(){
		return this.name;
	}
}
