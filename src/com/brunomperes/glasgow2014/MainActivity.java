package com.brunomperes.glasgow2014;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Data.generateSports();
		Intent intent = new Intent(this, SportListActivity.class);
	    startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void openInterface1(View v) {
		Intent intent = new Intent(this, SportTabActivity.class);
	    startActivity(intent);
	}

	public void openInterface2(View v) {
		Intent intent = new Intent(this, SportDropdownActivity.class);
	    startActivity(intent);
	}

	public void openInterface3(View v) {
		Intent intent = new Intent(this, SportListActivity.class);
	    startActivity(intent);
	}

	public void toast(String message) {
		Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
	}

}
